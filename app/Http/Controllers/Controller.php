<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function sendResponseSuccess(?array $data, int $http_code): JsonResponse
    {
        return response()->json([
            'meta' => [
                'success'   => true,
                'errors'    => []
            ],
            'data' => $data
        ], $http_code);
    }

    public function sendResponseError(?array $errors, int $http_code): JsonResponse
    {
        return response()->json([
            'meta' => [
                'success'   => true,
                'errors'    => $errors
            ],
        ], $http_code);
    }

    public function sendResponseSuccessWithResource(JsonResource $jsonResource, int $http_code): JsonResponse
    {
        return response()->json([
            'meta' => [
                'success'   => true,
                'errors'    => []
            ],
            'data' => $jsonResource
        ], $http_code);
    }
}
