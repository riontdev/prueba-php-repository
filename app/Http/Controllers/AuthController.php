<?php

namespace App\Http\Controllers;

use App\DTO\User as DTOUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRegisterRequest;
use App\Repository\UserRepository;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller {

    public function __construct(
        public readonly UserRepository $userRepository
    )
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function login(AuthLoginRequest $request): JsonResponse
    {
        $credentials = $request->only('user_name', 'password');
        $user_check = $this->userRepository->findUserName($request->user_name);

        if (!Hash::check($request->password, $user_check->password)) {
            return $this->sendResponseError([
                "Password incorrect for: {$user_check->user_name}"
            ], 401);
        }

        $token = Auth::attempt($credentials);
        if (!$token) {
            return $this->sendResponseError(['Unauthorized'], 401);
        }

        $this->userRepository->updateLastLogin(auth()->user()->id);

        return $this->sendResponseSuccess([
            'token'             => $token,
            'minutes_to_expire' => Auth::factory()->getTTL()
        ], 201);
    }

    public function register(AuthRegisterRequest $request): JsonResponse
    {
        $dto_user = new DTOUser(null, $request->email, $request->user_name, $request->password);
        $this->userRepository->create($dto_user, $request->password, $request->role);

        $token = Auth::attempt(['user_name' => $dto_user->user_name, 'password' => $dto_user->password]);

        $this->userRepository->updateLastLogin(auth()->user()->id);
        
        return $this->sendResponseSuccess([
            'token'             => $token,
            'minutes_to_expire' => Auth::factory()->getTTL()
        ], 201);
    }

    public function logout(Request $request): JsonResponse
    {
        Auth::logout();
        return $this->sendResponseSuccess(null, 201);
    }

    public function refresh(): JsonResponse
    {
        return $this->sendResponseSuccess([
            'token' => Auth::refresh(),
            'type' => 'bearer',
        ], 201);
    }
}
