<?php

namespace App\Http\Controllers;

use App\DTO\Lead;
use App\Http\Requests\StoreLeadRequest;
use App\Http\Resources\LeadCollection;
use App\Http\Resources\LeadResource;
use App\Repository\LeadRepository;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LeadController extends Controller
{
    public function __construct(
        public readonly LeadRepository $leadRepository
    )
    {
        $this->middleware('auth:api');
    }

    public function index(): ResourceCollection
    {
        $owner = auth()->user()->role == 'agent' ? auth()->user()->id : null;
        $leads = $this->leadRepository->getAll($owner);
        return new LeadCollection($leads);
    }

    public function store(StoreLeadRequest $request): JsonResponse
    {
        $lead  = new Lead(null, $request->name, $request->source, $request->owner, auth()->user()->id, null);
        $data = $this->leadRepository->create($lead);
        return  $this->sendResponseSuccessWithResource(new LeadResource($data), 201);
    }

    public function show(Request $request, int $id): JsonResponse
    {
        $lead = $this->leadRepository->find($id);

        if (empty($lead))
        {
            return $this->sendResponseError(['No lead found'], 404);
        }

        return $this->sendResponseSuccessWithResource(new LeadResource($lead), 201);
    }
}
