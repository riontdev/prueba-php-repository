<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseRequest extends FormRequest
{

    protected function failedValidation(Validator $validator): void
    {
        throw new HttpResponseException(response()->json(['meta' => ['success' => false, 'errors' => $validator->errors()->all()]], 400));
    }//end failedValidation()

    protected function failedAuthorization(): void
    {
        throw new HttpResponseException(response()->json(['meta' => ['success' => false, 'errors' => ['no authorize']]], 400));
    }
}
