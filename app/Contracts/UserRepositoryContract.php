<?php

declare(strict_types=1);

namespace App\Contracts;

use App\DTO\User;

interface UserRepositoryContract {
    public function create(User $user, string $password, string $role): User;
    public function findUserName(string $user_name): ?User;
    public function updateLastLogin(int $id): void;
}