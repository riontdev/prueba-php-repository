<?php

declare(strict_types=1);

namespace App\Contracts;

use App\DTO\Lead;
use App\Http\Resources\LeadCollection;

interface LeadRepositoryContract {
    public function getAll(?int $owner): LeadCollection;
    public function create(Lead $lead): Lead;
    public function find(int $id): ?Lead;
}