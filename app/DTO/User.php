<?php

declare(strict_types=1);

namespace App\DTO;

use App\Models\User as ModelsUser;

class User
{
    public function __construct(
        public readonly ?int $id, 
        public readonly string $email, 
        public readonly string $user_name,
        public readonly ?string $password,
        ) {
    }

    static public function make(ModelsUser $model): User
    {
        return new User($model->id, $model->email, $model->user_name, null); 
    }

    public function toArray()
    {
        return [
            'id'            => $this->id,
            'email'         => $this->email,
            'user_name'     => $this->user_name,
            'password'      => $this->password,
        ];
    }
}
