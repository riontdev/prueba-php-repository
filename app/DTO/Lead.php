<?php

declare(strict_types=1);

namespace App\DTO;

use App\Models\Lead as ModelsLead;
use Carbon\Carbon;

class Lead
{
    public function __construct(
        public readonly ?int $id, 
        public readonly string $name, 
        public readonly string $source,
        public readonly int $owner,
        public readonly int $created_by,
        public readonly ?Carbon $created_at,
        ) {
    }

    static public function make(ModelsLead $model): Lead
    {
        return new Lead($model->id, $model->name, $model->source, $model->owner, $model->created_by, $model->created_at); 
    }

    public function toArray()
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'source'        => $this->source,
            'owner'         => $this->owner,
            'created_by'    => $this->created_by,
            'created_at'    => $this->created_at,
        ];
    }
}
