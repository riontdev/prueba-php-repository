<?php

declare(strict_types=1);

namespace App\Repository;

use App\Contracts\UserRepositoryContract;
use App\DTO\User;
use App\Models\User as ModelsUser;
use Exception;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryContract
{
    public function findUserName(string $user_name): ?User
    {
        $db_user = ModelsUser::whereUserName($user_name)->first();
        if (!empty($db_user)) {
            return new User($db_user->id, $db_user->email, $db_user->user_name, $db_user->password);
        }

        return null;
    }

    public function create(User $user, string $password, string $role): User
    {
        $user_model = ModelsUser::create([
            'email'             =>  $user->email,
            'user_name'         =>  $user->user_name,
            'password'          =>  Hash::make($password),
            'role'              =>  $role
        ]);

        return User::make($user_model);
    }

    public function updateLastLogin(int $id): void
    {
        $user_model = ModelsUser::find($id);
        if (empty($user_model))
        {
            throw new Exception('No found User');
        }

        $user_model->last_login = now();
        $user_model->save();
    }

}
