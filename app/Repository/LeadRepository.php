<?php

declare(strict_types=1);

namespace App\Repository;

use App\Contracts\LeadRepositoryContract;
use App\DTO\Lead;
use App\Http\Resources\LeadCollection;
use App\Models\Lead as ModelsLead;
use Illuminate\Support\Facades\Cache;

class LeadRepository implements LeadRepositoryContract
{

    public function getAll(?int $owner): LeadCollection
    {
        $db_lead = [];

        if (!empty($owner) && $owner > 0) {
            $db_lead = Cache::remember('leads.owner.'.$owner, 60, function () use($owner) {
                return ModelsLead::whereOwner($owner)->get();
            });
        } else {
            $db_lead = Cache::remember('leads.all', 60, function () use($owner) {
                return ModelsLead::all();
            });
        }

       return new LeadCollection($db_lead);
    }

    public function find(int $id): ?Lead
    {
        $db_lead = ModelsLead::find($id);
        if (!empty($db_lead)) {
            return Lead::make($db_lead);
        }

        return null;
    }

    public function create(Lead $lead): Lead
    {
        $db_lead = ModelsLead::create([
            'name'          =>  $lead->name,
            'source'        =>  $lead->source,
            'owner'         =>  $lead->owner,
            'created_by'    =>  $lead->created_by,
        ]);

        return Lead::make($db_lead);
    }
}
