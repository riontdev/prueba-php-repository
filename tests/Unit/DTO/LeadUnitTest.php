<?php

namespace Tests\Unit\DTO;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\DTO\Lead;
use App\Models\Lead as ModelsLead;
use Tests\TestCase;

class LeadUnitTest extends TestCase
{ 
    /**
     * A basic test example.
     */
    public function testMake(): void
    {
        $model = new ModelsLead();
        $model->id = 1;
        $model->name = 'testname';
        $model->owner = 1;
        $model->source = 'test';
        $model->created_by = 1;
        $model->created_at = now();


        $lead = Lead::make($model);
        $this->assertEquals($model->id, $lead->id);
        $this->assertEquals($model->name, $lead->name);
        $this->assertEquals($model->owner, $lead->owner);
        $this->assertEquals($model->created_by, $lead->created_by);
        $this->assertEquals($model->created_at, $lead->created_at);
    }

    public function testToArray(): void
    {
        $lead = new Lead(1, 'test', 'test-source', 1, 1, now());
        $array =  $lead->toArray();

        $this->assertEquals($array['id'], $lead->id);
        $this->assertEquals($array['name'], $lead->name);
        $this->assertEquals($array['owner'], $lead->owner);
        $this->assertEquals($array['created_by'], $lead->created_by);
        $this->assertEquals($array['created_at'], $lead->created_at);
    }
}
