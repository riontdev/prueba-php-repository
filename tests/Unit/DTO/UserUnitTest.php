<?php

namespace Tests\Unit\DTO;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\DTO\User;
use App\Models\User as ModelsUser;
use Tests\TestCase;

class UserUnitTest extends TestCase
{ 
     /**
     * A basic test example.
     */
    public function testMake(): void
    {
        $model = new ModelsUser();
        $model->id = 1;
        $model->user_name = 'testname';
        $model->email = 'test@test.com';
        $model->is_active = 1;
        $model->password = 'test';
        $model->role = 'manager';


        $user = User::make($model);
        $this->assertEquals($model->id, $user->id);
        $this->assertEquals($model->user_name, $user->user_name);
        $this->assertEquals($model->email, $user->email);
        $this->assertNull($user->password);
    }

    public function testToArray(): void
    {
        $user = new User(1, 'test@test.com', 'test', 'test-pass');
        $array =  $user->toArray();

        $this->assertEquals($array['id'], $user->id);
        $this->assertEquals($array['user_name'], $user->user_name);
        $this->assertEquals($array['password'], $user->password);
        $this->assertEquals($array['email'], $user->email);
    }
}
