<?php

namespace Tests\Unit\Http\Controllers;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\DTO\Lead as DTOLead;
use App\Http\Controllers\LeadController;
use App\Http\Requests\StoreLeadRequest;
use App\Http\Resources\LeadCollection;
use App\Models\Lead;
use App\Models\User;
use App\Repository\LeadRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class LeadControllerUnitTest extends TestCase
{

    private $repo_mock;
    private $unit;

    public function setup(): void
    {
        parent::setup();
        $this->repo_mock = $this->mock(LeadRepository::class);
        $this->unit = new LeadController($this->repo_mock);
    }

    /**
     * A basic test example.
     */
    public function testIndexSuccess(): void
    {
        $model = new User();
        $model->id = 1000;
        $model->role = 'agent';

        $lead = new Lead();
        $lead->id = 1;
        $lead->name = 'test-name';
        $lead->owner = 1;
        $lead->created_by = 1;
        $lead->created_at = now();

        $this->repo_mock->shouldReceive('getAll')
        ->with(1000)
        ->once()
        ->andReturn(new LeadCollection([$lead]));

        Auth::shouldReceive('user')
        ->times(2)
        ->andReturn($model);


        $body = $this->unit->index();
        $response = $body->first();

        $this->assertEquals(1, $response->id);
    }

    public function testIndexSuccessManager(): void
    {
        $model = new User();
        $model->id = 2000;
        $model->role = 'manager';

        $lead = new Lead();
        $lead->id = 1;
        $lead->name = 'test-name';
        $lead->owner = 1;
        $lead->created_by = 1;
        $lead->created_at = now();

        $this->repo_mock->shouldReceive('getAll')
        ->with(null)
        ->once()
        ->andReturn(new LeadCollection([$lead]));

        Auth::shouldReceive('user')
        ->once()
        ->andReturn($model);


        $body = $this->unit->index();
        $response = $body->first();

        $this->assertEquals(1, $response->id);
    }

    public function testStoreSuccess(): void
    {
        $model = new User();
        $model->id = 2000;
        $model->role = 'manager';

        $lead = new DTOLead(1, 'test-lead-name', 'test-source', 1, 1 , now());

        $this->repo_mock->shouldReceive('create')
        ->once()
        ->andReturn($lead);

        Auth::shouldReceive('user')
        ->once()
        ->andReturn($model);

        $request = new StoreLeadRequest([
            'name' => 'test-lead-name',
            'source' => 'test-source',
            'owner' => 1,
        ]);

        $body = $this->unit->store($request)->getContent();
        $response = json_decode($body);

        $this->assertEquals(1, $response->data->id);
        $this->assertEquals($request->name, $response->data->name);
        $this->assertEquals($request->owner, $response->data->owner);
    }

    public function testFindSuccess(): void
    {
        $lead = new DTOLead(1, 'test-lead-name', 'test-source', 1, 1 , now());

        $this->repo_mock->shouldReceive('find')
        ->once()
        ->andReturn($lead);

        $body = $this->unit->show(new Request(), 1)->getContent();
        $response = json_decode($body);

        $this->assertEquals(1, $response->data->id);
        $this->assertEquals($lead->name, $response->data->name);
        $this->assertEquals($lead->owner, $response->data->owner);
        $this->assertEquals($lead->source, $response->data->source);
        $this->assertEquals($lead->created_by, $response->data->created_by);
    }
}
