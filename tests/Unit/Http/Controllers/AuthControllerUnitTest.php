<?php

namespace Tests\Unit\Http\Controllers;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\DTO\User;
use App\Http\Controllers\AuthController;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRegisterRequest;
use App\Models\User as ModelsUser;
use App\Repository\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mockery\LegacyMockInterface;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;

class AuthControllerUnitTest extends TestCase
{

    private $repo_mock;
    private $unit;
    
    public function setup(): void
    {
        parent::setup();
        $this->repo_mock = $this->mock(UserRepository::class);
        $this->unit = new AuthController($this->repo_mock);
    }
    /**
     * A basic test example.
     */
    public function testLoginSuccess(): void
    {
       
        $model = new ModelsUser();
        $model->id = 1;

        $this->repo_mock->shouldReceive('findUserName')
        ->once()
        ->andReturn(new User(1, 'test@test.com', 'test-user', 'test'));

        $this->repo_mock->shouldReceive('updateLastLogin')
        ->once();

        Hash::partialMock()
        ->shouldReceive('check')
        ->once()
        ->andReturn(true);

        $token = 'token';
        Auth::shouldReceive('attempt')
        ->once()
        ->andReturn($token);

        Auth::shouldReceive('user')
        ->once()
        ->andReturn($model);

        Auth::shouldReceive('factory')
        ->once()
        ->andReturnSelf()
        ->getMock()
        ->shouldReceive('getTTL')
        ->andReturn(1440);

        $request = new AuthLoginRequest(["user_name" => 'test-user', 'password' => 'test']);
        $body = $this->unit->login($request)->getContent();

        $response = json_decode($body);

        $this->assertEquals($token, $response->data->token);
        $this->assertEquals(1440, $response->data->minutes_to_expire);
    }

    public function testLoginErrorPassword(): void
    {

        $model = new ModelsUser();
        $model->id = 1;

        $this->repo_mock->shouldReceive('findUserName')
        ->once()
        ->andReturn(new User(1, 'test@test.com', 'test-user', 'test'));


        Hash::partialMock()
        ->shouldReceive('check')
        ->once()
        ->andReturn(false);


        $request = new AuthLoginRequest(["user_name" => 'test-user', 'password' => 'test']);
        $body = $this->unit->login($request)->getContent();;

        $response = json_decode($body);

        $this->assertEquals('Password incorrect for: test-user', $response->meta->errors[0]);
    }

    public function testLoginErrorGenerateToken(): void
    {

        $model = new ModelsUser();
        $model->id = 1;

        $this->repo_mock->shouldReceive('findUserName')
        ->once()
        ->andReturn(new User(1, 'test@test.com', 'test-user', 'test'));

        Hash::partialMock()
        ->shouldReceive('check')
        ->once()
        ->andReturn(true);

        Auth::shouldReceive('attempt')
        ->once()
        ->andReturn(null);

        $request = new AuthLoginRequest(["user_name" => 'test-user', 'password' => 'test']);
        $body = $this->unit->login($request)->getContent();;

        $response = json_decode($body);

        $this->assertEquals('Unauthorized', $response->meta->errors[0]);
    }

    public function testRegisterSuccess(): void
    {
        $model = new ModelsUser();
        $model->id = 1;

        $this->repo_mock->shouldReceive('create')
        ->once()
        ->andReturn(new User(1, 'test@test.com', 'test-user', 'test'));


        $this->repo_mock->shouldReceive('updateLastLogin')
        ->once();

        $token = 'token';
        Auth::shouldReceive('attempt')
        ->once()
        ->andReturn($token);

        Auth::shouldReceive('user')
        ->once()
        ->andReturn($model);

         Auth::shouldReceive('factory')
        ->once()
        ->andReturnSelf()
        ->getMock()
        ->shouldReceive('getTTL')
        ->andReturn(1440);

        $request = new AuthRegisterRequest(["user_name" => 'test-user', 'password' => 'test', 'email' => 'test@test.com', 'role' => 'manager']);
        $body = $this->unit->register($request)->getContent();;

        $response = json_decode($body);

        $this->assertEquals($token, $response->data->token);
        $this->assertEquals(1440, $response->data->minutes_to_expire);
    }

    public function testLogoutSuccess(): void
    {

        Auth::shouldReceive('logout')
        ->once()
        ->andReturn();

        $body = $this->unit->logout(new Request())->getContent();;

        $response = json_decode($body);

        $this->assertNull($response->data);
    }

    public function testRefreshSuccess(): void
    {

        Auth::shouldReceive('refresh')
        ->once()
        ->andReturn('test-token-refresh');

        $body = $this->unit->refresh()->getContent();;

        $response = json_decode($body);

        $this->assertEquals('test-token-refresh',$response->data->token);
        $this->assertEquals('bearer',$response->data->type);
    }


}
